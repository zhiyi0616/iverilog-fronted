# 核心组件

此文件根源于GitHub上开源的[iverilog源码](https://github.com/steveicarus/iverilog)进行核心组件的移植。

​    **编译器组件**

- The compiler driver (driver/)用户入口， 主要提供命令解析。
- The preprocessor(ivlpp/) 预处理器，处理一部分反引号开头的命令。
- The Core compiler (this directory)  根目录，仿真核心， 编译成‘ivl.exe'。

下图为核心组件的依赖关系：

<img src="doc_imgs/iverilog-core-dependency.jpg" style="zoom:67%;" />





# 剥离工作

**更改工程根目录文件夹下面的 autoconf**

​	删除autoconf文件下面16行这一段代码：

```makefile
	echo "Precompiling vhdlpp/lexor_keyword.gperf"
	(cd vhdlpp ; gperf -o -i 7 --ignore-case -C -k 1-4,6,9,\$ -H keyword_hash -N check_identifier -t ./lexor_keyword.gperf > lexor_keyword.cc )
```

因为不需要vhdlpp这个文件夹，那么就要改变对应的autoconf规则。



**更改工程根目录文件夹下面的configure.in文件 **

删除configure.in文件下面的关于其它文件配置信息的代码

```
AC_CONFIG_HEADER(vhdlpp/vhdlpp_config.h)
AC_CONFIG_HEADER(vvp/config.h)
AC_CONFIG_HEADER(vpi/vpi_config.h)
AC_CONFIG_HEADER(libveriuser/config.h)
AC_CONFIG_HEADER(tgt-vvp/vvp_config.h)
AC_CONFIG_HEADER(tgt-vhdl/vhdl_config.h)
AC_CONFIG_HEADER(tgt-pcb/pcb_config.h)
```

将configure.in源码的

```
AC_OUTPUT(Makefile ivlpp/Makefile vhdlpp/Makefile vvp/Makefile vpi/Makefile driver/Makefile driver-vpi/Makefile cadpli/Makefile libveriuser/Makefile tgt-null/Makefile tgt-stub/Makefile tgt-vvp/Makefile tgt-vhdl/Makefile tgt-fpga/Makefile tgt-verilog/Makefile tgt-pal/Makefile tgt-vlog95/Makefile tgt-pcb/Makefile tgt-blif/Makefile tgt-sizer/Makefile)
```

删除其它多余的文件输出，修改为：

```
AC_OUTPUT(Makefile ivlpp/Makefile driver/Makefile )
```



**修改根目录下面Makefile.in文件**

将40行的源码修改为：

```makefile
SUBDIRS = ivlpp driver
```

删除42-48行的代码

```makefile
NOTUSED = tgt-fpga tgt-pal tgt-verilog

ifeq (@MINGW32@,yes)
SUBDIRS += driver-vpi
else
NOTUSED += driver-vpi
endif
```



**在工程根目录下加入源码静态库文件夹libmisc**



**在根目录下创建一个vvp文件夹，里面就单独放一个 ’ivl_dlfcn.h‘ 文件**



因为在根目录下面 ’vpi_modules.cc‘ 这个C++文件代码当中有：
```c++
#include "vvp/ivl_dlfcn.h"
```
整个核心组件编译的时候需要用到这个C++文件，为了不改变目录的结构就单独创建一个 vvp 文件夹目录。

# iverilog-core 文件编译说明

在MSYS2上面进行编译,说明链接：[MSYS2上使用iverilog](https://iverilog.fandom.com/wiki/Installation_using_MSYS2)

输入编译命令，按照顺序

1. 进入目录
    ```shell
    cd "iverilog-core"
    ```

2. 编译 autoconf.sh 文件
    ```shell
    sh autoconf.sh
    ```

3. 输入命令配置目录下的 configure.in文件
    ```shell
    ./configure
    ```

4. 编译iverilog
    ```shell
    make
    ```

# iverilog-core编译流程框图

为了更好地展现核心组件移植工作，为此特地列出了剥离前iVerilog源码编译步骤框图和剥离后iverilog-core核心组件编译框图进行对比。

- 剥离前iVerilog源码编译步骤框图

<img src="doc_imgs/iverilog-complie.jpg" style="zoom:67%;" />


- 剥离后iVerilog-Core核心组件编译步骤框图

<img src="doc_imgs/iverilog-core-complie.jpg" style="zoom:67%;" />